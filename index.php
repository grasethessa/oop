<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Hewan("shaun");

echo "Nama hewan : ".$sheep->name."<br>"; // "shaun"
echo "Berkaki : ". $sheep->legs."<br>"; // 4
echo "Cold blooded? : ".$sheep->cold_blooded."<br><br>"; // "no"

$frog = new Katak("buduk");

echo "Nama hewan : ".$frog->name."<br>"; // "buduk"
echo "Berkaki : ". $frog->legs."<br>"; // 4
echo "Cold blooded? : ".$frog->cold_blooded."<br>"; // "no"
echo $frog->jump();
echo "<br><br>";

$ape = new Kera("kera sakti");

echo "Nama hewan : ".$ape->name."<br>"; // "kera sakti"
echo "Berkaki : ". $ape->legs."<br>"; // 2
echo "Cold blooded? : ".$ape->cold_blooded."<br>"; // "no"
echo $ape->yell();

?>

